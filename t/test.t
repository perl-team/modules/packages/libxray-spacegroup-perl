#!/usr/bin/perl -I../blib/lib/

use Test::Simple tests => 19;

use Xray::SpaceGroup;

############################################################
## Tests of extracting information from the database
my $sg = Xray::SpaceGroup -> new({group=>'pc'});
ok($sg->group eq 'p c',                                     "canonicalize space group");
ok($sg->given eq 'pc',                                      "given group retained");
ok($sg->schoenflies eq 'c_s^2',                             "obtain schoenflies symbol");
ok($sg->class eq 'monoclinic',                              "obtain crystal class");
ok($sg->setting eq 'b_unique_1',                            "crystal setting");

my @positions = $sg->positions;
my ($x, $y, $z) = (0.2, 0.3, 0.4);
my  @pos1 = (eval "$positions[1]->[0]",
             eval "$positions[1]->[1]",
             eval "$positions[1]->[2]"  ); # ("$x", "-$y", "$z+1/2")
ok($pos1[0] == 0.2 && $pos1[1] == -0.3 && $pos1[2] == 0.9,  "compute from positions");

$sg = Xray::SpaceGroup -> new({group=>'f d -3 m'});
my @list = $sg->bravais;
ok($#list == 8,                                             "obtain Bravais translations");

$sg = Xray::SpaceGroup -> new({group=>'f d -3 m'});
@list = $sg->positions;
ok($#list == 47,                                            "obtain positions");

############################################################
## Tests of parsing space group symbols
$sg = Xray::SpaceGroup -> new({group=>'pm3m'});
ok($sg->group eq 'p m -3 m',                                "no spaces in symbol");

$sg = Xray::SpaceGroup -> new({group=>'Pm3M'});
ok($sg->group eq 'p m -3 m',                                "mixed case in symbol");

$sg = Xray::SpaceGroup -> new({group=>'p   m  3         m'});
ok($sg->group eq 'p m -3 m',                                "lots of spaces in symbol");

$sg = Xray::SpaceGroup -> new({group=>"pm\t3 \t m"});
ok($sg->group eq 'p m -3 m',                                "tabs in symbol");

$sg = Xray::SpaceGroup -> new({group=>'o_h^1'});
ok($sg->group eq 'p m -3 m',                                "shoenflies forward");

$sg = Xray::SpaceGroup -> new({group=>'o^1_h'});
ok($sg->group eq 'p m -3 m',                                "shoenflies backward");

$sg = Xray::SpaceGroup -> new({group=>'perovskite'});
ok($sg->group eq 'p m -3 m',                                "nickname");

$sg = Xray::SpaceGroup -> new({group=>221});
ok($sg->group eq 'p m -3 m',                                "number");

$sg = Xray::SpaceGroup -> new({group=>'p 4/m -3 2/m'});
ok($sg->group eq 'p m -3 m',                                "full symbol");

$sg = Xray::SpaceGroup -> new({group=>'p4/ m  -3   2 /m'});
ok($sg->group eq 'p m -3 m',                                "full symbol, weird spacing");

$sg = Xray::SpaceGroup -> new({group=>'p 6_3 m c'});
ok($sg->group eq 'p 63 m c',                                "underscore for subscript");
