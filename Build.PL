use strict;
use warnings;
use Module::Build;

my $build = Module::Build
  -> new(
	 create_readme      => 1,
	 ##create_makefile_pl => 'traditional',
	 license            => 'perl',
	 module_name        => 'Xray::SpaceGroup',
	 dist_author        => 'Bruce Ravel <bravel AT bnl DOT gov>',
	 dist_abstract      => "Object oriented interface to a space groups database",
	 requires => {
		      'version'		 => 0,
		      'Readonly'	 => 0,
		      'Class::Std'	 => 0,
		      'List::MoreUtils'	 => 0,
		      'Regexp::List'	 => 0,
		     },
	 PL_files           => {
				'data/space_groups.db.PL'  => 'data/space_groups.db',
			       },
	 db_files           => {
				'data/space_groups.db'  => 'lib/Xray/SpaceGroup/space_groups.db',
			       },
	 recommends         => {},
	 sign               => 0,
	);
$build->add_build_element('db');
$build->create_build_script;

