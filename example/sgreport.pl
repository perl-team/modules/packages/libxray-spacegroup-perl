#!/usr/bin/perl -I../blib/lib/

use Xray::SpaceGroup;
use Term::ReadLine;

my $term    = new Term::ReadLine 'demeter';
my $sg      = q{};
my $version = join("\n", q{}, "Xray::SpaceGroup version $Xray::SpaceGroup::VERSION,",
		   "copyright (c) 2008 Bruce Ravel",
		   "bravel AT bnl DOT gov", q{});
my $prompt  = "Enter a space group symbol or q=quit > ";

while ( defined ($_ = $term->readline($prompt)) ) {
  exit if ($_ =~ m{\Aq}i);
  next if ($_ =~ m{\A\s*\z});
  print($version), next if ($_ =~ m{\Av}i);
  $sg = Xray::SpaceGroup -> new($_);
  if ($sg -> warning) {
    print "That wasn't a space group.\n";
  } else {
    print $/, $sg -> report;
  };
};
